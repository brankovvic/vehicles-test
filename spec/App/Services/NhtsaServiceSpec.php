<?php

namespace spec\App\Services;

use App\Services\Interfaces\ApiService;
use App\Services\NhtsaService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class NhtsaServiceSpec extends ObjectBehavior
{
    function let(ApiService $apiService)
    {
        $this->beConstructedWith($apiService);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(NhtsaService::class);
    }

    function it_should_return_empty_results($apiService)
    {
        $response = ['Count' => 0, 'Results' => [],];

        $apiService->queryApiForCarInformation(1000, 'test', 'test')->willReturn($response);

        $this->fetchVehiclesByYearManufacturerModel(1000, 'test', 'test')->shouldReturn($response);
    }

    function it_should_return_correct_vehicle_data($apiService)
    {
        $response = [
            'Count' => 2,
            'Results' =>
                [
                    0 =>
                        [
                            'Description' => '2013 Acura RDX SUV 4WD',
                            'VehicleId' => 7731,
                        ],
                    1 =>
                        [
                            'Description' => '2013 Acura RDX SUV FWD',
                            'VehicleId' => 7520,
                        ],
                ],
        ];

        $apiService->queryApiForCarInformation(2013, 'Acura', 'rdx')->willReturn($response);

        $this->fetchVehiclesByYearManufacturerModel(2013, 'Acura', 'rdx')->shouldReturn($response);
    }

    function it_should_return_response_with_crash_rating($apiService)
    {
        $response = [
            'Count' => 2,
            'Results' =>
                [
                    0 =>
                        [
                            'Description' => '2013 Acura RDX SUV 4WD',
                            'VehicleId' => 7731,
                            "CrashRating"=>"Not Rated",
                        ],
                    1 =>
                        [
                            'Description' => '2013 Acura RDX SUV FWD',
                            'VehicleId' => 7520,
                            "CrashRating"=>"Not Rated",
                        ],
                ],
        ];

        $apiService->queryApiForCarInformation(2013, 'Acura', 'rdx')->willReturn($response);

        $this->fetchVehiclesByYearManufacturerModel(2013, 'Acura', 'rdx')->shouldReturn($response);
    }
}
