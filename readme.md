Test Application For Vehicles Information
==============

Welcome to my test application for fetching vehicles information.

Application is very simple. It uses model, year and manufacturer to fetch vehicle information, free https://one.nhtsa.gov/webapi/api is used to
fetch information.

Application is using laravel framework(https://laravel.com)
with PHP version PHP 7.1.7

PHPUnit 7.0 and PHPSpec 4.0 are used for application testing

Composer is used Dependency Manager!


Setup Application
----------------------------

After cloning the project use composer to install dependencies

.. code-block:: console

    $ composer install

After composer finishes installing all the dependencies, copy env file and run command to generate key

.. code-block:: console

    $cp .env.example .env
    $php artisan key:generate

we do not use any database or extra parameters so modifications are not needed

Running Tests
-----------------------------

If all the dependencies are installed tests needs to be run to insure that application is working as intended

Run phpspec tests:

.. code-block:: console

    $php vendor/bin/phpspec run

Run phpunit tests:

.. code-block:: console

    $php vendor/bin/phpunit tests

If all tests are successful you're now ready to use application.


Running Application
-----------------------------
To see a real-live application page in action, start the PHP built-in web server with
command:

.. code-block:: console

    $ php artisan serve

Then, browse to http://127.0.0.1:8000 and test api calls

Enjoy!
