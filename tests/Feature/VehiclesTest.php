<?php

use Tests\TestCase;

class VehiclesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEmptyResponseReturnedByApi()
    {
        $response = $this->get(route('index', [1000, 'test', 'test']));

        $response->assertJson(
            [
                'Count' => 0,
                'Results' => [],
            ]
        );
    }

    public function testCorrectResponseByModelManufacturerAndModel()
    {

        $response = $this->get(route('index', [2013, 'Acura', 'rdx']));

        $response->assertJson(
            [
                'Count' => 2,
                'Results' =>
                    [
                        0 =>
                            [
                                'Description' => '2013 Acura RDX SUV 4WD',
                                'VehicleId' => 7731,
                            ],
                        1 =>
                            [
                                'Description' => '2013 Acura RDX SUV FWD',
                                'VehicleId' => 7520,
                            ],
                    ],
            ]
        );
    }

    public function testIncorrectApiCall()
    {
        $response = $this->get(route('index', [2013, 'Ford', 'Crown Victoria']));

        $response->assertJson(
            [
                'Count' => 0,
                'Results' => [],
            ]
        );
    }

    public function testPostApiCallRequestWithCorrectData()
    {
        $response = $this->postJson(
            route('getVehicles'),
            ["modelYear" => 2015, "manufacturer" => "Audi", "model" => "A3"]
        );

        $response->assertJson(
            [
                'Count' => 4,
                'Results' =>
                    [
                        0 =>
                            [
                                'Description' => '2015 Audi A3 4 DR AWD',
                                'VehicleId' => 9403,
                            ],
                        1 =>
                            [
                                'Description' => '2015 Audi A3 4 DR FWD',
                                'VehicleId' => 9408,
                            ],
                        2 =>
                            [
                                'Description' => '2015 Audi A3 C AWD',
                                'VehicleId' => 9405,
                            ],
                        3 =>
                            [
                                'Description' => '2015 Audi A3 C FWD',
                                'VehicleId' => 9406,
                            ],
                    ],
            ]
        );
    }

    public function test_response_with_crash_rating()
    {
        $response = $this->get(route('index', [2013, 'Acura', 'rdx']) . "?withRating=true");

        $response->assertJson(
            [
                'Count' => 2,
                'Results' =>
                    [
                        0 =>
                            [
                                'Description' => '2013 Acura RDX SUV 4WD',
                                'VehicleId' => 7731,
                                'CrashRating' => 'Not Rated',
                            ],
                        1 =>
                            [
                                'Description' => '2013 Acura RDX SUV FWD',
                                'VehicleId' => 7520,
                                'CrashRating' => 'Not Rated',
                            ],
                    ],
            ]
        );
    }
}
