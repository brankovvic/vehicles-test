<?php

namespace App\Services\Interfaces;

interface ApiService
{
    /**
     * Returns Car Information
     *
     * @param int $modelYear
     * @param string $manufacturer
     * @param string $model
     *
     * @return array|null
     */
    public function queryApiForCarInformation(int $modelYear, string $manufacturer= "", string $model = ""): ?array;

    /**
     * Returns full api to query api parameters
     * @param int $modelYear
     * @param string $manufacturer
     * @param string $model
     *
     * @return string
     */
    public function getFullApiRouteWithParameters(int $modelYear, string $manufacturer= "", string $model = ""): string;

    /**
     * @param int $vehicleId
     *
     * @return array|null
     */
    public function queryApiByVehicleId(int $vehicleId): ?array;
}
