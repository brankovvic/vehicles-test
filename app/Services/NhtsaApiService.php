<?php

namespace App\Services;

use App\Services\Interfaces\ApiService;

class NhtsaApiService implements ApiService
{
    static $API_URL = "https://one.nhtsa.gov/webapi/api/SafetyRatings/";

    /**
     * Returns Car Information
     *
     * @param int $modelYear
     * @param string $manufacturer
     * @param string $model
     *
     * @return array|null
     */
    public function queryApiForCarInformation(
        int $modelYear,
        string $manufacturer = "",
        string $model = ""
    ): ?array {
        $response = @file_get_contents($this->getFullApiRouteWithParameters($modelYear, $manufacturer, $model));

        return ($response) ? json_decode($response, true) : null;
    }

    /**
     * Returns full api to query api parameters
     *
     * @param int $modelYear
     * @param string $manufacturer
     * @param string $model
     *
     * @return string
     */
    public function getFullApiRouteWithParameters(
        int $modelYear,
        string $manufacturer = "",
        string $model = ""
    ): string {
        return self::$API_URL . "modelyear/{$modelYear}/make/{$manufacturer}/model/{$model}";
}

    /**
     * @param int $vehicleId
     *
     * @return array|null
     */
    public function queryApiByVehicleId(int $vehicleId): ?array
    {
        $response = @file_get_contents(self::$API_URL . "VehicleId/{$vehicleId}");

        return ($response) ? json_decode($response, true) : null;
    }
}
