<?php

namespace App\Services;

use App\Services\Interfaces\ApiService;

class NhtsaService
{
    static $DEFAULT_RESPONSE = ['Count' => 0, 'Results' => []];
    /**
     * @var ApiService
     */
    private $apiService;

    public function __construct(ApiService $apiService)
    {

        $this->apiService = $apiService;
    }

    /**
     * @param int $modelYear
     * @param string $manufacturer
     * @param string $model
     *
     * @return array|null
     */
    public function fetchVehiclesByYearManufacturerModel(
        int $modelYear,
        string $manufacturer = "",
        string $model = ""
    ): ?array {
        $response = $this->apiService->queryApiForCarInformation($modelYear, $manufacturer, $model);

        if (is_null($response)) {
            return self::$DEFAULT_RESPONSE;
        }

        if (is_array($response) && !empty($response['Message'])) {
            unset($response['Message']);
        }

        //Change response structure keys
        return $this->replaceKeys('VehicleDescription', 'Description', $response);

    }

    public function fetchVehiclesByYearManufacturerModelWithCrashRating(
        int $modelYear,
        string $manufacturer = "",
        string $model = ""
    ) {

        $response = $this->apiService->queryApiForCarInformation($modelYear, $manufacturer, $model);

        if (is_null($response)) {
            return self::$DEFAULT_RESPONSE;
        }

        if (is_array($response) && !empty($response['Message'])) {
            unset($response['Message']);
        }

        foreach ($response['Results'] as $index=>$result) {
            $vehicleResponse = $this->apiService->queryApiByVehicleId($result['VehicleId']);

            if (!is_array($vehicleResponse)
                || empty($vehicleResponse['Results'])
                || empty($vehicleResponse['Results'][0]['OverallRating'])) {

                $response['Results'][$index]['CrashRating'] = $vehicleResponse['Results'][0]['OverallRating'];
            } else {
                $response['Results'][$index]['CrashRating'] = $vehicleResponse['Results'][0]['OverallRating'];
            }
        }
        //Here we can change the keys in the loop to prevent double looping over the same data set
        //but for the next implementation changes i will leave it with the current one
        return $this->replaceKeys('VehicleDescription', 'Description', $response);
    }

    /**
     * Replace keys for the given array
     *
     * @param $oldKey
     * @param $newKey
     * @param array $input
     *
     * @return array
     */
    private function replaceKeys($oldKey, $newKey, array $input): array
    {
        $return = [];
        foreach ($input as $key => $value) {
            if ($key === $oldKey) {
                $key = $newKey;
            }

            if (is_array($value)) {
                $value = $this->replaceKeys($oldKey, $newKey, $value);
            }

            $return[$key] = $value;
        }

        return $return;
    }
}
