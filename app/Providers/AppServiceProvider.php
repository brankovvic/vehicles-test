<?php

namespace App\Providers;

use App\Services\Interfaces\ApiService;
use App\Services\NhtsaApiService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->instance(ApiService::class, new NhtsaApiService());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
