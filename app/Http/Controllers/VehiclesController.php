<?php

namespace App\Http\Controllers;

use App\Http\Requests\VehicleRequest;
use App\Services\NhtsaService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VehiclesController extends Controller
{
    /**
     * @var NhtsaService
     */
    private $nhtsaService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NhtsaService $nhtsaService)
    {

        $this->nhtsaService = $nhtsaService;
    }

    /**
     * @param $modelYear
     * @param $manufacturer
     * @param $model
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($modelYear, $manufacturer, $model, Request $request)
    {

        if ($request->has('withRating') && $request->get('withRating') === "true") {
            return response()->json(
                $this->nhtsaService->fetchVehiclesByYearManufacturerModelWithCrashRating(
                    $modelYear,
                    $manufacturer,
                    $model
                )
            );
        }


        return response()->json(
            $this->nhtsaService->fetchVehiclesByYearManufacturerModel(
                $modelYear,
                $manufacturer,
                $model
            )
        );
    }

    public function getVehicles(VehicleRequest $vehicleRequest)
    {
        return response()->json( $this->nhtsaService->fetchVehiclesByYearManufacturerModel(
            $vehicleRequest->json('modelYear'),
            $vehicleRequest->json('manufacturer'),
            $vehicleRequest->json('model')
        ));
    }
}
